const Loading = () => {
  return (
    <div className="display-center loadingIcon">
      <i className="fa fa-spinner fa-spin fa-3x fa-fw"></i>
    </div>
  );
};

export default Loading;
