import { Typography } from '@material-ui/core';

export default function Resources() {
  return (
    <div
      style={{
        margin: '40px 0',
      }}
    >
      <Typography variant="h4">Resources</Typography>
    </div>
  );
}
