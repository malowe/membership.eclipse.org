import { Typography } from '@material-ui/core';

export default function FAQs() {
  return (
    <div
      style={{
        margin: '40px 0',
      }}
    >
      <Typography variant="h4">FAQs</Typography>
    </div>
  );
}
